package main

import (
	"log"
	"net/http"
	"os"

	itemrp "gitlab.com/fandyputram/checkout-service/repository/item"
	orderrp "gitlab.com/fandyputram/checkout-service/repository/order"

	itemuc "gitlab.com/fandyputram/checkout-service/usecase/item"
	orderuc "gitlab.com/fandyputram/checkout-service/usecase/order"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"gitlab.com/fandyputram/checkout-service/graph"
	"gitlab.com/fandyputram/checkout-service/graph/generated"

	"gitlab.com/fandyputram/checkout-service/pkg"
)

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	db, err := pkg.InitDB()
	if err != nil {
		log.Print("Error init db:", err)
		panic(err)
	}

	orderRP := orderrp.Init(orderrp.Dependencies{
		Db: db,
	})

	itemRP := itemrp.Init(itemrp.Dependencies{
		Db: db,
	})

	orderUC := orderuc.Init(orderuc.Dependencies{
		ItemRepo:  itemRP,
		OrderRepo: orderRP,
	})

	itemUC := itemuc.Init(itemuc.Dependencies{
		ItemRepo: itemRP,
	})

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{OrderUC: orderUC, ItemUC: itemUC}}))

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Print(http.ListenAndServe(":"+port, nil))
}

func populate() {
}
