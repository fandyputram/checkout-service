package pkg

import (
	"database/sql"
	"fmt"

	"gitlab.com/fandyputram/checkout-service/constant"

	_ "github.com/lib/pq"
)

func InitDB() (*sql.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		constant.Host, constant.Port, constant.User, constant.Password, constant.Dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		// panic(err)
		return db, err
	}
	// defer db.Close()
	return db, nil
}
