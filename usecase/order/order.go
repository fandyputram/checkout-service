package order

import (
	"context"
	"log"
	"math"

	"gitlab.com/fandyputram/checkout-service/constant"
	model "gitlab.com/fandyputram/checkout-service/graph/model"
)

func (impl *OrderImpl) CreateOrder(ctx context.Context) (*model.Order, error) {
	order, err := impl.dep.OrderRepo.CreateOrder(ctx)
	if err != nil {
		log.Printf("failed create order, with err : %v ", err)
		return order, err
	}
	return order, nil
}

func (impl *OrderImpl) AddItem(ctx context.Context, id int, sku string) (*model.Order, error) {
	var order *model.Order

	item, err := impl.dep.ItemRepo.GetItemBySKU(ctx, sku)
	if err != nil {
		log.Printf("failed get item by item sku, with item sku : %s and err : %v ", sku, err)
		return order, err
	}

	err = impl.dep.OrderRepo.AddItemByOrderID(ctx, id, item)
	if err != nil {
		log.Printf("failed add item to order by order id, with order id : %d and item sku : %s and err : %v ", id, sku, err)
		return order, err
	}
	return order, nil
}

func (impl *OrderImpl) Checkout(ctx context.Context, id int) (*model.Order, error) {
	var order model.Order

	order.ID = id
	items, err := impl.dep.ItemRepo.GetItemsByOrderID(ctx, id)
	if err != nil {
		log.Printf("failed get order by order id, with order id : %d and err : %v ", id, err)
		return &order, err
	}
	order.Items = items

	impl.calculateOrder(ctx, &order)

	return &order, nil
}

//calculateOrder function is to calculate the total price of all item in the order including promo calculation
func (impl *OrderImpl) calculateOrder(ctx context.Context, order *model.Order) {
	for _, item := range order.Items {
		order.Price = order.Price + (item.Price * float64(item.Quantity))
	}
	impl.calculatePromo(ctx, order)
}

//calculatePromo function is to calculate the promo price to deduct from total price in an order
func (impl *OrderImpl) calculatePromo(ctx context.Context, order *model.Order) {
	type itemCount struct {
		Count int
		Price float64
	}
	itemMap := map[string]*itemCount{}

	//loop through item and map item in order to itemMap, index 0 for item count in order and index 1 for item price
	for _, item := range order.Items {
		itemMap[item.Sku] = &itemCount{
			Count: item.Quantity,
			Price: item.Price,
		}
	}

	//Promo rule 1 -> Each sale of a MacBook Pro comes with a free Raspberry Pi B
	if val1, ok := itemMap[constant.SKUMacBookPro]; ok && val1.Count > 0 {
		if val2, ok := itemMap[constant.SKURaspberryPiB]; ok && val2.Count > 0 {
			count := math.Min(float64(val1.Count), float64(val2.Count))
			order.Price = order.Price - (itemMap[constant.SKURaspberryPiB].Price * count)
		}
	}

	//Promo rule 2 -> Buy 3 Google Homes for the price of 2, temp is the count of google homes item in order multiples of three
	if val, ok := itemMap[constant.SKUGoogleHome]; ok && (val.Count/3) >= 1 {
		temp := val.Count / 3
		order.Price = order.Price - val.Price*float64(temp)
	}

	//Promo rule 3 -> Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers
	if val, ok := itemMap[constant.SKUAlexaSpeaker]; ok && val.Count > 3 {
		order.Price = order.Price - (0.1 * float64(val.Count) * (val.Price))
	}
}
