package order

import (
	itemrp "gitlab.com/fandyputram/checkout-service/repository/item"
	orderrp "gitlab.com/fandyputram/checkout-service/repository/order"
)

type Dependencies struct {
	OrderRepo orderrp.IOrder
	ItemRepo  itemrp.IItem
}

type OrderImpl struct {
	dep Dependencies
}

func Init(dep Dependencies) IOrder {
	return &OrderImpl{
		dep: dep,
	}
}
