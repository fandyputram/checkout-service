package order

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/fandyputram/checkout-service/constant"
	model "gitlab.com/fandyputram/checkout-service/graph/model"
	mockitem "gitlab.com/fandyputram/checkout-service/repository/item/mocks"
	mockorder "gitlab.com/fandyputram/checkout-service/repository/order/mocks"
)

var dummyContext = context.TODO()
var dummyString = "test"
var dummyInt = int(1)
var dummyFloat64 = float64(100)
var dummyErrorMessage = "this is error message"
var dummyError = fmt.Errorf(dummyErrorMessage)
var dummyItem = model.Item{Sku: dummyString, Name: dummyString, Price: dummyFloat64, Quantity: dummyInt}
var dummyItems = []*model.Item{
	&dummyItem}
var dummyOrder = model.Order{ID: dummyInt, Items: dummyItems, Price: dummyFloat64}
var mockOrderRepo = &mockorder.IOrder{}
var mockItemRepo = &mockitem.IItem{}

func TestOrderImpl_CreateOrder(t *testing.T) {

	type fields struct {
		dep Dependencies
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *model.Order
		wantErr bool
		mocks   func()
	}{
		{
			name: "1. Success",
			fields: fields{
				dep: Dependencies{
					OrderRepo: mockOrderRepo,
				},
			},
			args: args{
				ctx: dummyContext,
			},
			want:    &dummyOrder,
			wantErr: false,
			mocks: func() {
				mockOrderRepo.On("CreateOrder", dummyContext).Return(&dummyOrder, nil).Once()
			},
		},
		{
			name: "2. Error",
			fields: fields{
				dep: Dependencies{
					OrderRepo: mockOrderRepo,
				},
			},
			args: args{
				ctx: dummyContext,
			},
			want:    &dummyOrder,
			wantErr: true,
			mocks: func() {
				mockOrderRepo.On("CreateOrder", dummyContext).Return(&dummyOrder, dummyError).Once()
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mocks != nil {
				tt.mocks()
			}

			impl := &OrderImpl{
				dep: tt.fields.dep,
			}
			got, err := impl.CreateOrder(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("OrderImpl.CreateOrder() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OrderImpl.CreateOrder() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderImpl_AddItem(t *testing.T) {
	type fields struct {
		dep Dependencies
	}
	type args struct {
		ctx context.Context
		id  int
		sku string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *model.Order
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			impl := &OrderImpl{
				dep: tt.fields.dep,
			}
			got, err := impl.AddItem(tt.args.ctx, tt.args.id, tt.args.sku)
			if (err != nil) != tt.wantErr {
				t.Errorf("OrderImpl.AddItem() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OrderImpl.AddItem() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderImpl_Checkout(t *testing.T) {
	dummyOrderError := model.Order{ID: dummyInt}
	type fields struct {
		dep Dependencies
	}
	type args struct {
		ctx context.Context
		id  int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *model.Order
		wantErr bool
		mocks   func()
	}{
		{
			name: "1. Success",
			fields: fields{
				dep: Dependencies{
					ItemRepo: mockItemRepo,
				},
			},
			args: args{
				ctx: dummyContext,
				id:  dummyInt,
			},
			wantErr: false,
			want:    &dummyOrder,
			mocks: func() {
				mockItemRepo.On("GetItemsByOrderID", dummyContext, dummyInt).Return(dummyItems, nil).Once()
			},
		},
		{
			name: "2. Error",
			fields: fields{
				dep: Dependencies{
					ItemRepo: mockItemRepo,
				},
			},
			args: args{
				ctx: dummyContext,
				id:  dummyInt,
			},
			wantErr: true,
			want:    &dummyOrderError,
			mocks: func() {
				mockItemRepo.On("GetItemsByOrderID", dummyContext, dummyInt).Return(dummyItems, dummyError).Once()
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mocks != nil {
				tt.mocks()
			}
			impl := &OrderImpl{
				dep: tt.fields.dep,
			}
			got, err := impl.Checkout(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("OrderImpl.Checkout() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OrderImpl.Checkout() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderImpl_calculateOrder(t *testing.T) {
	type fields struct {
		dep Dependencies
	}
	type args struct {
		ctx   context.Context
		order *model.Order
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		expectedPrice float64
	}{
		{
			name: "1. Test",
			args: args{
				ctx: dummyContext,
				order: &model.Order{
					ID: dummyInt,
					Items: []*model.Item{
						{
							Sku:      constant.SKUMacBookPro,
							Price:    1000,
							Quantity: 1,
						},
						{
							Sku:      constant.SKURaspberryPiB,
							Price:    10,
							Quantity: 2,
						},
						{
							Sku:      constant.SKUAlexaSpeaker,
							Price:    100,
							Quantity: 4,
						},
						{
							Sku:      constant.SKUGoogleHome,
							Price:    200,
							Quantity: 3,
						},
					},
				},
			},
			expectedPrice: 1770,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			impl := &OrderImpl{
				dep: tt.fields.dep,
			}
			impl.calculateOrder(tt.args.ctx, tt.args.order)
			if tt.args.order.Price != tt.expectedPrice {
				t.Errorf("Expected price() = %v, actual %v", tt.expectedPrice, tt.args.order.Price)
			}
		})
	}
}
