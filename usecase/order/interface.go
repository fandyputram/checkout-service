package order

import (
	"context"

	model "gitlab.com/fandyputram/checkout-service/graph/model"
)

type IOrder interface {
	AddItem(ctx context.Context, id int, sku string) (*model.Order, error)
	Checkout(ctx context.Context, id int) (*model.Order, error)
	CreateOrder(ctx context.Context) (*model.Order, error)
}
