package item

import (
	itemrp "gitlab.com/fandyputram/checkout-service/repository/item"
)

type Dependencies struct {
	ItemRepo itemrp.IItem
}

type ItemImpl struct {
	dep Dependencies
}

func Init(dep Dependencies) IItem {
	return &ItemImpl{
		dep: dep,
	}
}
