package item

import (
	"context"

	model "gitlab.com/fandyputram/checkout-service/graph/model"
)

type IItem interface {
	GetItems(ctx context.Context, sku string) ([]*model.Item, error)
}
