package item

import (
	"context"
	"log"

	model "gitlab.com/fandyputram/checkout-service/graph/model"
)

//GetItems func is to get item data, if sku data is passed then it will return item with that sku, otherwise it will return list of all item
func (impl *ItemImpl) GetItems(ctx context.Context, sku string) ([]*model.Item, error) {
	var items []*model.Item
	if sku != "" {
		item, err := impl.dep.ItemRepo.GetItemBySKU(ctx, sku)
		if err != nil {
			log.Printf("failed get item by item sku, with item sku : %s and err : %v ", sku, err)
			return items, err
		}
		items = append(items, item)
		return items, err
	}
	items, err := impl.dep.ItemRepo.GetItems(ctx)
	if err != nil {
		log.Printf("failed get items, with err : %v ", err)
		return items, err
	}
	return items, err
}
