package item

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	mockitem "gitlab.com/fandyputram/checkout-service/repository/item/mocks"

	model "gitlab.com/fandyputram/checkout-service/graph/model"
)

func TestItemImpl_GetItems(t *testing.T) {
	mockItemRepo := &mockitem.IItem{}

	dummyContext := context.TODO()
	dummyString := "test"
	dummyInt := int(1)
	dummyFloat64 := float64(1)
	dummyErrorMessage := "this is error message"
	dummyError := fmt.Errorf(dummyErrorMessage)
	dummyItem := model.Item{Sku: dummyString, Name: dummyString, Price: dummyFloat64, Quantity: dummyInt}
	dummyItems := []*model.Item{
		&dummyItem}

	type fields struct {
		dep Dependencies
	}
	type args struct {
		ctx context.Context
		sku string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []*model.Item
		wantErr bool
		mocks   func()
	}{
		{
			name: "1. Success with sku parameter",
			fields: fields{
				dep: Dependencies{
					ItemRepo: mockItemRepo,
				},
			},
			args: args{
				ctx: dummyContext,
				sku: dummyString,
			},
			want:    dummyItems,
			wantErr: false,
			mocks: func() {
				mockItemRepo.On("GetItemBySKU", dummyContext, dummyString).Return(&dummyItem, nil).Once()
			},
		},
		{
			name: "2. Success without sku parameter",
			fields: fields{
				dep: Dependencies{
					ItemRepo: mockItemRepo,
				},
			},
			args: args{
				ctx: dummyContext,
				sku: "",
			},
			want:    dummyItems,
			wantErr: false,
			mocks: func() {
				mockItemRepo.On("GetItems", dummyContext).Return(dummyItems, nil).Once()
			},
		},
		{
			name: "3. Error with sku",
			fields: fields{
				dep: Dependencies{
					ItemRepo: mockItemRepo,
				},
			},
			args: args{
				ctx: dummyContext,
				sku: dummyString,
			},
			want:    nil,
			wantErr: true,
			mocks: func() {
				mockItemRepo.On("GetItemBySKU", dummyContext, dummyString).Return(&dummyItem, dummyError).Once()
			},
		},
		{
			name: "4. Error without sku",
			fields: fields{
				dep: Dependencies{
					ItemRepo: mockItemRepo,
				},
			},
			args: args{
				ctx: dummyContext,
				sku: "",
			},
			want:    nil,
			wantErr: true,
			mocks: func() {
				mockItemRepo.On("GetItems", dummyContext).Return(nil, dummyError).Once()
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mocks != nil {
				tt.mocks()
			}

			impl := &ItemImpl{
				dep: tt.fields.dep,
			}
			got, err := impl.GetItems(tt.args.ctx, tt.args.sku)
			if (err != nil) != tt.wantErr {
				t.Errorf("ItemImpl.GetItems() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ItemImpl.GetItems() = %v, want %v", got, tt.want)
			}
		})
	}
}
