package order

import (
	"context"

	"gitlab.com/fandyputram/checkout-service/graph/model"
)

const (
	queryInsertOrder = `
	INSERT INTO orders DEFAULT VALUES
	RETURNING order_id`
)

//AddItemByOrderID is a function to add an item to order with determined order id
func (impl *OrderRP) AddItemByOrderID(ctx context.Context, id int, item *model.Item) error {
	return nil
}

//CreateOrder is a function to create a new order, it will insert a new order data and return the new order id
func (impl *OrderRP) CreateOrder(ctx context.Context) (*model.Order, error) {
	var order *model.Order
	err := impl.dep.Db.QueryRow(queryInsertOrder).Scan(&order.ID)
	if err != nil {
		return order, err
	}
	return order, nil
}
