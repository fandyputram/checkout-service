package order

import (
	"context"

	"gitlab.com/fandyputram/checkout-service/graph/model"
)

type IOrder interface {
	AddItemByOrderID(ctx context.Context, id int, item *model.Item) error
	CreateOrder(ctx context.Context) (*model.Order, error)
}
