package order

import "database/sql"

type Dependencies struct {
	Db *sql.DB
}

type OrderRP struct {
	dep Dependencies
}

func Init(dep Dependencies) IOrder {
	return &OrderRP{
		dep: dep,
	}
}
