package item

import (
	"context"

	"gitlab.com/fandyputram/checkout-service/graph/model"
)

type IItem interface {
	GetItemBySKU(ctx context.Context, sku string) (*model.Item, error)
	GetItems(ctx context.Context) ([]*model.Item, error)
	GetItemsByOrderID(ctx context.Context, id int) ([]*model.Item, error)
}
