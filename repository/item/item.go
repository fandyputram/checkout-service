package item

import (
	"context"

	"gitlab.com/fandyputram/checkout-service/graph/model"
)

const (
	baseQueryGetItem = `
		SELECT 
			i.sku, i.name, i.price, i.quantity
		FROM 
			items i`

	queryGetItemBySKU = baseQueryGetItem + `WHERE i.sku = $1`

	queryGetItemByOrderID = `
		SELECT i.sku, i.name, i.price, oi.quantity, 
			FROM orders_items oi
			LEFT JOIN items i ON oi.sku = i.sku
		WHERE oi.order_id = $1
	`
)

func (impl *ItemRP) GetItemBySKU(ctx context.Context, sku string) (*model.Item, error) {
	var item *model.Item
	rows, err := impl.dep.Db.Query(queryGetItemBySKU, sku)
	defer rows.Close()
	if err != nil {
		return item, err
	}
	rows.Scan(
		&item.Sku,
		&item.Name,
		&item.Price,
		&item.Quantity)

	return item, nil
}

func (impl *ItemRP) GetItems(ctx context.Context) ([]*model.Item, error) {
	var items []*model.Item
	rows, err := impl.dep.Db.Query(baseQueryGetItem)
	defer rows.Close()
	if err != nil {
		return items, err
	}

	for rows.Next() {
		var item *model.Item
		rows.Scan(
			&item.Sku,
			&item.Name,
			&item.Price,
			&item.Quantity)
		items = append(items, item)
	}

	return items, nil
}

func (impl *ItemRP) GetItemsByOrderID(ctx context.Context, id int) ([]*model.Item, error) {
	var items []*model.Item
	rows, err := impl.dep.Db.Query(queryGetItemByOrderID, id)
	defer rows.Close()
	if err != nil {
		return items, err
	}

	for rows.Next() {
		var item *model.Item
		rows.Scan(
			&item.Sku,
			&item.Name,
			&item.Price,
			&item.Quantity)
		items = append(items, item)
	}

	return items, nil
}
