package item

import "database/sql"

type Dependencies struct {
	Db *sql.DB
}

type ItemRP struct {
	dep Dependencies
}

func Init(dep Dependencies) IItem {
	return &ItemRP{
		dep: dep,
	}
}
