package constant

const (
	SKUMacBookPro   = "43N23P"
	SKUGoogleHome   = "120P90"
	SKUAlexaSpeaker = "A304SD"
	SKURaspberryPiB = "234234"
)
