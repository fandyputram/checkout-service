/* Description
orders table : to store order data
*/
DROP TABLE IF EXISTS "orders";
DROP SEQUENCE IF EXISTS orders_seq;
CREATE SEQUENCE orders_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
CREATE TABLE "public"."orders"
(
    "order_id"               bigint    DEFAULT nextval('orders_seq')    NOT NULL,
    CONSTRAINT "orders_pkey" PRIMARY KEY ("order_id")
) WITH (oids = false);

/* Description
orders table : to store order data
*/
DROP TABLE IF EXISTS "orders_items";
CREATE TABLE "public"."orders_items"
(
    "order_id"  bigint                  NOT NULL,
    "sku"       CHARACTER VARYING(255)  NOT NULL,
    "quantity"  bigint                  NOT NULL,
) WITH (oids = false);

/* Description
items table : to store item data
*/
DROP TABLE IF EXISTS "items";
CREATE TABLE "public"."items"
(
    "sku"       CHARACTER VARYING(255)  NOT NULL,
    "name"      CHARACTER VARYING(255)  NOT NULL,
    "price"     NUMERIC(15, 3)          NOT NULL,
    "quantity"  bigint                  NOT NULL,
    CONSTRAINT "items_pkey" PRIMARY KEY ("sku")
) WITH (oids = false);