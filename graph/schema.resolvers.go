package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/fandyputram/checkout-service/graph/generated"
	"gitlab.com/fandyputram/checkout-service/graph/model"
)

func (r *mutationResolver) AddItem(ctx context.Context, id int, sku string) (*model.Order, error) {
	order, err := r.OrderUC.AddItem(ctx, id, sku)
	return order, err
}

func (r *mutationResolver) Checkout(ctx context.Context, id int) (*model.Order, error) {
	order, err := r.OrderUC.Checkout(ctx, id)
	return order, err
}

func (r *mutationResolver) CreateOrder(ctx context.Context) (int, error) {
	order, err := r.OrderUC.CreateOrder(ctx)
	return order.ID, err
}

func (r *queryResolver) Item(ctx context.Context, sku string) (*model.Item, error) {
	items, err := r.ItemUC.GetItems(ctx, sku)
	return items[0], err
}

func (r *queryResolver) Items(ctx context.Context) ([]*model.Item, error) {
	items, err := r.ItemUC.GetItems(ctx, "")
	return items, err
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
