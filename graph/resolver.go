package graph

import (
	item "gitlab.com/fandyputram/checkout-service/usecase/item"
	order "gitlab.com/fandyputram/checkout-service/usecase/order"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	OrderUC order.IOrder
	ItemUC  item.IItem
}
